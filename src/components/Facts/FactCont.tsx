import * as React from "react";
import "./Fakt.css";

export interface IFactContProps {
  fakt: string;
}
interface IFactContState { }

class FactCont extends React.Component<IFactContProps, IFactContState> {


  constructor(props: IFactContProps, state: IFactContState) {
    super(props, state);
  }

  public render() {
    let styleClasses: string = "fakt";
    if (this.props.fakt !== "") {
      styleClasses += " bordered";
    }
    return <div className={styleClasses}>{this.props.fakt}</div>;
  }
}

export default FactCont;
